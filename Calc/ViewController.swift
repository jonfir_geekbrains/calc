//
//  ViewController.swift
//  Calc
//
//  Created by Евгений Елчев on 15.08.2018.
//  Copyright © 2018 jonfir. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lableView: UILabel!
    @IBOutlet var numberButtons: [UIButton]!
    
    var ferstNumber = ""
    var operation = ""
    
    @IBAction func pressNumberButton(_ sender: UIButton) {
        if lableView.text == "0" {
           lableView.text?.removeAll()
        }
        lableView.text! += sender.titleLabel!.text!
    }
    @IBAction func clearButtonPressed(_ sender: Any) {
        lableView.text = "0"
    }
    @IBAction func dev(_ sender: Any) {
        ferstNumber = lableView.text!
        lableView.text = "0"
        operation = "/"
    }
    @IBAction func mul(_ sender: Any) {
        ferstNumber = lableView.text!
        lableView.text = "0"
        operation = "*"
    }
    @IBAction func sub(_ sender: Any) {
        ferstNumber = lableView.text!
        lableView.text = "0"
        operation = "-"
    }
    @IBAction func sum(_ sender: Any) {
        ferstNumber = lableView.text!
        lableView.text = "0"
        operation = "+"
    }
    @IBAction func rav(_ sender: Any) {
        let secondNumber = lableView.text!
        
        let fNumber = Double(ferstNumber) ?? 0
        let sNumber = Double(secondNumber) ?? 0
        
        var result = 0.0
        switch operation {
        case "/":
            result = fNumber / sNumber
        case "*":
            result = fNumber * sNumber
        case "-":
            result = fNumber - sNumber
        case "+":
            result = fNumber + sNumber
        default:
            print("Действие неизвестно")
        }
        
        lableView.text = String(result)
    }
    
}

